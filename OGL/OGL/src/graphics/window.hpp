#ifndef WINDOW_H
#define WINDOW_H
#include <SDL.h>
#include <GL/glew.h>
#include <iostream>

#include "../utility/util.hpp"

#include <vec2.hpp>


struct attributeStruct {
	int gl_major_version = 3;
	int gl_minor_version = 3;

	int gl_red_buffer_size = 8;
	int gl_green_buffer_size = 8;
	int gl_blue_buffer_size = 8;
	int gl_alpha_buffer_size = 8;
	int gl_buffer_size = 32;

	bool gl_doublebuffer = true;

	int gl_depth_size = 16;
	int gl_stencil_size = 0;

	int gl_accum_red_buffer_size = 0;
	int gl_accum_green_buffer_size = 0;
	int gl_accum_blue_buffer_size = 0;
	int gl_accum_alpha_buffer_size = 0;

	bool sdl_stereo = false;

	int gl_multisamplebuffers = 0;
	int gl_multisamplesamples = 0;
};


class Window
{
public:
	enum windowPos : u32 {
		pos_centered = SDL_WINDOWPOS_CENTERED,
		pos_undefined = SDL_WINDOWPOS_UNDEFINED
	};

	enum fullscrType : u32 {
		t_fullscreen = SDL_WINDOW_FULLSCREEN,
		t_fullscreen_desktop = SDL_WINDOW_FULLSCREEN_DESKTOP
	};

	Window(const char* title, 
		const i32 width, 
		const i32 height, 
		const i32 x = pos_centered,
		const i32 y = pos_centered,
		const attributeStruct atrst = attributeStruct());

	~Window();

	void setFullscreen(bool fullscreen, fullscrType type = t_fullscreen);
	
	void render();
	void clear();

	glm::vec2 getSize();

	SDL_Window* getSDLWindow();
	SDL_GLContext& getContext();

private:
	int m_width;
	int m_height;
	bool m_fullscreen;

	SDL_Window* m_sdl_window = nullptr;
	SDL_GLContext m_gl_context;
};

#endif