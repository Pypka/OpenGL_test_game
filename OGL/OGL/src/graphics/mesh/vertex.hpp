#pragma once
#include <glm.hpp>

class Vertex
{
public:
	Vertex();
	~Vertex();

	glm::vec3 pos;
	glm::vec2 uvcoord;
};