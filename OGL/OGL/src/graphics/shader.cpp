#include "shader.hpp"


bool checkShader(GLuint shader) {
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	char buffer[1024];
	glGetShaderInfoLog(shader, 1024, nullptr, buffer);

	std::cout << buffer << std::endl;

	if (status == GL_FALSE) return false;
	return true;
}


bool checkProgram(GLuint program) {
	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	
	char buffer[1024];
	glGetProgramInfoLog(program, 1024, nullptr, buffer);

	std::cout << buffer << std::endl;

	if (status == GL_FALSE) return false;
	return true;
}


std::string loadFile(std::string filename) {
	std::fstream fin(filename);

	std::cout << "Opening shader file " << filename << std::endl;

	std::string str;

	if (fin.is_open()) {
		std::string temp;
		while (std::getline(fin, temp)) {
			str = str + temp + "\n";
			temp = "";
		}
	}
	else {
		std::cout << "Failed to open shader file " << filename << std::endl;
	}

	fin.close();
	return str;
}


bool compileShader(GLuint& shader, std::string& source, GLenum shadertype) {
	const char* src = source.c_str();
	glShaderSource(shader, 1, &src, nullptr);
	glCompileShader(shader);
	if (!checkShader(shader)) return false;
	return true;
}


Shader::Shader() : shaderProgram(0), _isLinked(false)
{
	shaderProgram = glCreateProgram();
}


Shader::~Shader() {
	if (shaderProgram != 0)
	{
		glDeleteProgram(shaderProgram);
	}
}


void Shader::bind() {
	if (!isBound()) {
		glUseProgram(shaderProgram);
	}
}


void Shader::unbind() {
	if (isBound()) {
		glUseProgram(0);
	}
}


bool Shader::link() {
	if (shaderProgram == 0) {
		shaderProgram = glCreateProgram();
	}

	if (!_isLinked) {
		glLinkProgram(shaderProgram);
		if(!checkProgram(shaderProgram))
			return _isLinked = false;
	}

	return _isLinked = true;
}


bool Shader::attachShader(ShaderType shaderType, const std::string& filename) {
	GLuint shader = glCreateShader(static_cast<GLenum>(shaderType));

	if (shaderProgram == 0) {
		shaderProgram = glCreateProgram();
	}

	if (!compileShader(shader, loadFile(filename), static_cast<GLenum>(shaderType))) return false;

	glAttachShader(shaderProgram, shader);
	link();
	glDeleteShader(shader);
	return true;
}


void Shader::bindAttributeLocation(GLuint location, const std::string& attributeName) {
	glBindAttribLocation(shaderProgram, location, attributeName.c_str());
	_attributeLocations[attributeName] = location;
}


GLint Shader::getAttributeLocation(const std::string& attributeName) {
	auto found = _attributeLocations.find(attributeName);
	if (found != _attributeLocations.end())
	{
		return found->second;
	}

	auto location = glGetAttribLocation(shaderProgram, attributeName.c_str());
	_attributeLocations[attributeName] = location;
	return location;
}


GLint Shader::getUniformLocation(const std::string& uniformName) {
	auto found = _uniformLocations.find(uniformName);
	if (found != _uniformLocations.end())
	{
		return found->second;
	}

	auto location = glGetUniformLocation(shaderProgram, uniformName.c_str());
	_uniformLocations[uniformName] = location;
	return location;
}


void Shader::setUniform(const std::string& uniformName, f32 x) {
	printIsBound();
	glUniform1f(getUniformLocation(uniformName), x);
}


void Shader::setUniform(const std::string& uniformName, f32 x, f32 y) {
	printIsBound();
	glUniform2f(getUniformLocation(uniformName), x, y);
}


void Shader::setUniform(const std::string& uniformName, f32 x, f32 y, f32 z) {
	printIsBound();
	glUniform3f(getUniformLocation(uniformName), x, y, z);
}


void Shader::setUniform(const std::string& uniformName, f32 x, f32 y, f32 z, f32 w) {
	printIsBound();
	glUniform4f(getUniformLocation(uniformName), x, y, z, w);
}


void Shader::setUniform(const std::string& uniformName, i32 x) {
	printIsBound();
	glUniform1i(getUniformLocation(uniformName), x);
}


void Shader::setUniform(const std::string& uniformName, i32 x, i32 y) {
	printIsBound();
	glUniform2i(getUniformLocation(uniformName), x, y);
}


void Shader::setUniform(const std::string& uniformName, i32 x, i32 y, i32 z) {
	printIsBound();
	glUniform3i(getUniformLocation(uniformName), x, y, z);
}


void Shader::setUniform(const std::string& uniformName, i32 x, i32 y, i32 z, i32 w) {
	printIsBound();
	glUniform4i(getUniformLocation(uniformName), x, y, z, w);
}


void Shader::setUniform(const std::string& uniformName, bool value) {
	printIsBound();
	glUniform1i(getUniformLocation(uniformName), static_cast<i32>(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::mat3x3& value) {
	printIsBound();
	glUniformMatrix3fv(getUniformLocation(uniformName), 1, GL_FALSE, glm::value_ptr(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::mat4& value) {
	printIsBound();
	glUniformMatrix4fv(getUniformLocation(uniformName), 1, GL_FALSE, glm::value_ptr(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::mat4x3& value) {
	printIsBound();
	glUniformMatrix4x3fv(getUniformLocation(uniformName), 1, GL_FALSE, glm::value_ptr(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::vec2& value) {
	printIsBound();
	glUniform2fv(getUniformLocation(uniformName), 1, glm::value_ptr(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::vec3& value) {
	printIsBound();
	glUniform3fv(getUniformLocation(uniformName), 1, glm::value_ptr(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::vec4& value) {
	printIsBound();
	glUniform3fv(getUniformLocation(uniformName), 1, glm::value_ptr(value));
}


void Shader::setUniform(const std::string& uniformName, const glm::quat& value) {
	printIsBound();
	glUniform4fv(getUniformLocation(uniformName), 1, glm::value_ptr(value));
}


/*void Shader::setUniform(const std::string& uniformName, const ColorRGBA& value) {
	printIsBound();

	f32 r{ value.r };
	f32 g{ value.g };
	f32 b{ value.b };
	f32 a{ value.a };

	setUniform(uniformName, r, g, b, a);
}


void Shader::setUniform(const std::string& uniformName, const ColorRGB& value) {
	printIsBound();

	f32 r{ value.r };
	f32 g{ value.g };
	f32 b{ value.b };

	setUniform(uniformName, r, g, b);
}*/


bool Shader::isBound() const {
	GLint curShaderProgram = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &curShaderProgram);
	return (GLint)shaderProgram == curShaderProgram;
}


void Shader::printIsBound() const {
	if (!isBound()) {
		std::cout << "Shader is not bound !" << std::endl;
	}
}