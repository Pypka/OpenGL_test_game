#include "window.hpp"


void initAttributes(attributeStruct atrs) {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, atrs.gl_major_version);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, atrs.gl_minor_version);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, atrs.gl_red_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, atrs.gl_green_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, atrs.gl_blue_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, atrs.gl_alpha_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, atrs.gl_buffer_size);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, static_cast<i32>(atrs.gl_doublebuffer));

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, atrs.gl_depth_size);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, atrs.gl_stencil_size);

	SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE, atrs.gl_accum_red_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE, atrs.gl_accum_green_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE, atrs.gl_accum_blue_buffer_size);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE, atrs.gl_accum_alpha_buffer_size);

	SDL_GL_SetAttribute(SDL_GL_STEREO, static_cast<i32>(atrs.sdl_stereo));

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, atrs.gl_multisamplebuffers);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, atrs.gl_multisamplesamples);
}


Window::Window(const char* title, const i32 width, const i32 height, const i32 x, const i32 y, attributeStruct atrst) :
	m_width(width),
	m_height(height),
	m_fullscreen(false)
{
	initAttributes(atrst);
	/*SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	//SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, atrs.gl_depth_size);*/

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

	m_sdl_window = SDL_CreateWindow(title, x, y, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	m_gl_context = SDL_GL_CreateContext(m_sdl_window);
	GLenum e = glewInit();
	if (e != GLEW_OK) std::cout << "Failed to initialize GLEW - " << glewGetErrorString(e) << std::endl;

	std::string newtitle{ title + std::string("       OpenGL: ") + std::string(reinterpret_cast<const char*>(glGetString(GL_VERSION))) };

	SDL_SetWindowTitle(m_sdl_window, newtitle.c_str());
}


Window::~Window()
{
	SDL_DestroyWindow(m_sdl_window);
	SDL_GL_DeleteContext(m_gl_context);
	SDL_Quit();
}


void Window::setFullscreen(bool fullscreen, fullscrType type) {
	if (!m_fullscreen)
		SDL_SetWindowFullscreen(m_sdl_window, type);
	else
		SDL_SetWindowFullscreen(m_sdl_window, 0);
	m_fullscreen = fullscreen;
}


void Window::render() {
	SDL_GL_SwapWindow(m_sdl_window);
}


void Window::clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


SDL_Window* Window::getSDLWindow() {
	return m_sdl_window;
}


SDL_GLContext& Window::getContext() {
	return m_gl_context;
}


glm::vec2 Window::getSize() {
	return glm::vec2{ static_cast<float>(m_width), static_cast<float>(m_height) };
}