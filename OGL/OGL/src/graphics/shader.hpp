#pragma once
#include <GL/glew.h>

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#include <vec2.hpp>
#include <vec3.hpp>
#include <vec4.hpp>

#include <mat2x2.hpp>
#include <mat2x3.hpp>
#include <mat2x4.hpp>
#include <mat3x2.hpp>
#include <mat3x3.hpp>
#include <mat3x4.hpp>
#include <mat4x2.hpp>
#include <mat4x3.hpp>
#include <mat4x4.hpp>

#include <gtc/type_ptr.hpp>

#include "../utility/util.hpp"

std::string loadFile(std::string filename);

class Shader : NotCopyable
{
public:
	enum class ShaderType {
		vertexShader = GL_VERTEX_SHADER,
		fragmentShader = GL_FRAGMENT_SHADER,
		computeShader = GL_COMPUTE_SHADER,					//OpenGL 4.3 or higher
		geometryShader = GL_GEOMETRY_SHADER,				//OpenGL 3.3 or higher (suggested OpenGL 4.0)
		tessControlShader = GL_TESS_CONTROL_SHADER,			//OpenGL 4.0 or higher
		tessEvaluationShader = GL_TESS_EVALUATION_SHADER	//OpenGL 4.0 or higher
	};

	Shader();
	virtual ~Shader();

	bool attachShader(ShaderType shaderType, const std::string& filename);

	inline GLuint getProgram() const { return shaderProgram; }

	void bindAttributeLocation(GLuint location, const std::string& attributeName);
	GLint getAttributeLocation(const std::string& attributeName);
	GLint getUniformLocation(const std::string& uniformName);

	void setUniform(const std::string& uniformName, f32 x);
	void setUniform(const std::string& uniformName, f32 x, f32 y);
	void setUniform(const std::string& uniformName, f32 x, f32 y, f32 z);
	void setUniform(const std::string& uniformName, f32 x, f32 y, f32 z, f32 w);

	void setUniform(const std::string& uniformName, i32 x);
	void setUniform(const std::string& uniformName, i32 x, i32 y);
	void setUniform(const std::string& uniformName, i32 x, i32 y, i32 z);
	void setUniform(const std::string& uniformName, i32 x, i32 y, i32 z, i32 w);

	void setUniform(const std::string& uniformName, bool value);

	//TODO: add more matrix uniforms
	void setUniform(const std::string& uniformName, const glm::mat3x3& value);
	void setUniform(const std::string& uniformName, const glm::mat4& value);
	void setUniform(const std::string& uniformName, const glm::mat4x3& value);

	void setUniform(const std::string& uniformName, const glm::vec2& value);
	void setUniform(const std::string& uniformName, const glm::vec3& value);
	void setUniform(const std::string& uniformName, const glm::vec4& value);

	void setUniform(const std::string& uniformName, const glm::quat& value);

	//void setUniform(const std::string& uniformName, const ColorRGBA& value);
	//void setUniform(const std::string& uniformName, const ColorRGB& value);

	void bind();
	void unbind();
	bool isBound() const;
	bool link();

private:

	GLuint shaderProgram;

	bool _isLinked;
	std::map<std::string, GLint> _uniformLocations;
	std::map<std::string, GLint> _attributeLocations;

	void printIsBound() const;
};
