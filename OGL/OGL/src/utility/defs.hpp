#pragma once

#include <cstdint>
#include <cstddef>
#include <glm.hpp>


#ifdef _WIN32
#define MODE_WINDOW_32
#elif __UNIX__
#define MODE_UNIX_32
#endif


#ifdef _DEBUG
#define MODE_DEBUG
#else
#define MODE_RELEASE
#endif


//Vector coordinate 2D
using vec2_i8 = glm::i8vec2;
using vec2_i16 = glm::i16vec2;
using vec2_i32 = glm::i32vec2;
using vec2_i64 = glm::i64vec2;

using vec2_u8 = glm::u8vec2;
using vec2_u16 = glm::u16vec2;
using vec2_u32 = glm::u32vec2;
using vec2_u64 = glm::u64vec2;

using vec2 = glm::vec2;

using vec2_f32 = glm::f32vec2;
using vec2_f64 = glm::f64vec2;


//Vector coordinate 3D
using vec3_i8 = glm::i8vec3;
using vec3_i16 = glm::i16vec3;
using vec3_i32 = glm::i32vec3;
using vec3_i64 = glm::i64vec3;

using vec3_u8 = glm::u8vec3;
using vec3_u16 = glm::u16vec3;
using vec3_u32 = glm::u32vec3;
using vec3_u64 = glm::u64vec3;

using vec3 = glm::vec3;

using vec3_f32 = glm::f32vec3;
using vec3_f64 = glm::f64vec3;


//Quaternion
using quat_f32 = glm::highp_f32quat;
using quat_f64 = glm::highp_f64quat;

using quat = glm::quat;


//Typenames
using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using f32 = float;
using f64 = double;

using b8 = bool;