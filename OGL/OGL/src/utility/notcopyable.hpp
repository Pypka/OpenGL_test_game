#pragma once


class NotCopyable {
protected:
	NotCopyable() { }
private:
	NotCopyable(const NotCopyable&) = delete;
	NotCopyable& operator=(const NotCopyable&) = delete;

	NotCopyable(const NotCopyable&&) = delete;
	NotCopyable& operator=(const NotCopyable&&) = delete;
};